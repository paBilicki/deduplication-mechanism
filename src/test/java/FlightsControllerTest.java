import model.Discount;
import model.FlightResult;
import org.junit.Test;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class FlightsControllerTest {

    private static final LocalDateTime TODAY = LocalDateTime.now();
    private static final LocalDateTime YESTERDAY = TODAY.minusDays(1);
    private static final LocalDateTime TOMORROW = TODAY.plusDays(1);

    private FlightsController controller = new FlightsController();

    @Test
    public void mechanism_do_not_remove_unique() {
        List<FlightResult> uniques = uniqueFlights();
        List<FlightResult> results = controller.getFlightsWithNoDuplicates(uniques);

        resultsHaveBeenSortedButNothingWasRemoved(uniques, results);
    }

    @Test
    public void mechanism_remove_exactly_the_same() {
        List<FlightResult> clones = exactlyTheSameFlights();
        List<FlightResult> results = controller.getFlightsWithNoDuplicates(clones);

        containsOnlyTheNewestResult(results);

        FlightResult expectedResult = clones.get(0);
        assertThat(results).containsOnly(expectedResult);
    }

    @Test
    public void mechanism_remove_duplicates_with_meaningless_differences() {
        List<FlightResult> clones = onlyDifferentCurrencies();
        List<FlightResult> results = controller.getFlightsWithNoDuplicates(clones);

        containsOnlyTheNewestResult(results);

        FlightResult expectedResult = clones.get(0);
        assertThat(results).containsOnly(expectedResult);
    }

    @Test
    public void mechanism_keeps_the_newest_flight() {
        List<FlightResult> differentDates = unorderedDifferentDates();
        List<FlightResult> results = controller.getFlightsWithNoDuplicates(differentDates);

        containsOnlyTheNewestResult(results);

        FlightResult expectedResult = differentDates.get(2);
        assertThat(results).containsOnly(expectedResult);
    }

    @Test
    public void mechanism_recognizes_object_important_field() {
        List<FlightResult> differentDiscountPercentage = onlyDifferentDiscountPercentage();
        List<FlightResult> results = controller.getFlightsWithNoDuplicates(differentDiscountPercentage);

        resultsHaveBeenSortedButNothingWasRemoved(differentDiscountPercentage, results);
    }

    @Test
    public void mechanism_recognizes_object_not_important_field() {
        List<FlightResult> differentDiscountReason = onlyDifferentDiscountReason();
        List<FlightResult> results = controller.getFlightsWithNoDuplicates(differentDiscountReason);

        containsOnlyTheNewestResult(results);
        
        FlightResult expectedResult = differentDiscountReason.get(2);
        assertThat(results).containsOnly(expectedResult);
    }

    private void resultsHaveBeenSortedButNothingWasRemoved(List<FlightResult> input, List<FlightResult> results) {
        assertThat(results).as("No unique flight should be removed")
                .hasSize(input.size()).containsOnly(varArgs(input));
    }

    private void containsOnlyTheNewestResult(List<FlightResult> noDuplicates) {
        assertThat(noDuplicates).as("Should remove both duplicates").hasSize(1)
                .as("The kept result should be the newest")
                .extracting(FlightResult::getDepartureDateTime_USED_IN_CONFLICTS).contains(TOMORROW);
    }

    private FlightResult[] varArgs(List<FlightResult> list) {
        return list.stream().toArray(FlightResult[]::new);
    }

    private List<FlightResult> uniqueFlights() {
        return Arrays.asList(
                new FlightResult("a1", new Discount(10, "student"), "pln", YESTERDAY),
                new FlightResult("a2", new Discount(20, "summer off"), "eur", TODAY),
                new FlightResult("a3", new Discount(30, "winter off"), "gbp", TOMORROW)
        );
    }

    private List<FlightResult> exactlyTheSameFlights() {
        return Arrays.asList(
                new FlightResult("a1", new Discount(10, "student"), "pln", TOMORROW),
                new FlightResult("a1", new Discount(10, "student"), "pln", TODAY),
                new FlightResult("a1", new Discount(10, "student"), "pln", TODAY)
        );
    }

    private List<FlightResult> onlyDifferentCurrencies() {
        return Arrays.asList(
                new FlightResult("a1", new Discount(10, "student"), "pln", TOMORROW),
                new FlightResult("a1", new Discount(10, "student"), "eur", TODAY),
                new FlightResult("a1", new Discount(10, "student"), "gbp", TODAY)
        );
    }

    private List<FlightResult> onlyDifferentDiscountPercentage() {
        return Arrays.asList(
                new FlightResult("a1", new Discount(10, "random"), "pln", TODAY),
                new FlightResult("a1", new Discount(20, "random"), "pln", TODAY),
                new FlightResult("a1", new Discount(30, "random"), "pln", TODAY)
        );
    }

    private List<FlightResult> onlyDifferentDiscountReason() {
        return Arrays.asList(
                new FlightResult("a1", new Discount(10, "student"), "pln", YESTERDAY),
                new FlightResult("a1", new Discount(10, "summer off"), "pln", TODAY),
                new FlightResult("a1", new Discount(10, "last minute"), "pln", TOMORROW)
        );
    }

    private List<FlightResult> unorderedDifferentDates() {
        return Arrays.asList(
                new FlightResult("a1", new Discount(10, "student"), "pln", YESTERDAY),
                new FlightResult("a1", new Discount(10, "student"), "pln", TODAY),
                new FlightResult("a1", new Discount(10, "student"), "pln", TOMORROW)
        );
    }

}