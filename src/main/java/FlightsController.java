import model.FlightResult;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class FlightsController {

    public List<FlightResult> getFlightsWithNoDuplicates(List<FlightResult> withDupes){

        return withDupes
                .stream()
                .sorted(Comparator.comparing(FlightResult::getDepartureDateTime_USED_IN_CONFLICTS).reversed())
                .map(f -> FlightResultWrapper.from(f))
                .distinct()
                .map(FlightResultWrapper::getFlightResult)
                .collect(Collectors.toList());
    }
}
