package model;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class Discount {
    private final int percentage;
    private final String reason;

    public Discount(int percentage, String reason) {
        this.percentage = percentage;
        this.reason = reason;
    }

}
