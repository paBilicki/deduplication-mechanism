package model;

import lombok.Getter;
import lombok.ToString;

import java.time.LocalDateTime;

@Getter
@ToString
public class FlightResult {

    private final String supplier_IMPORTANT_PRIMITIVE_FIELD;
    private final Discount discount_IMPORTANT_FIELD_OBJECT;
    private final String currency_NOT_IMPORTANT;
    private final LocalDateTime departureDateTime_USED_IN_CONFLICTS;

    public FlightResult(String fabSupplierCode,
                        Discount discount,
                        String currencyCode,
                        LocalDateTime departureDateTime
    ) {

        this.supplier_IMPORTANT_PRIMITIVE_FIELD = fabSupplierCode;
        this.discount_IMPORTANT_FIELD_OBJECT = discount;
        this.departureDateTime_USED_IN_CONFLICTS = departureDateTime;
        this.currency_NOT_IMPORTANT = currencyCode;
    }
}


