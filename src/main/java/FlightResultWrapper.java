import lombok.Getter;
import model.FlightResult;

import java.util.Objects;

public class FlightResultWrapper {

    @Getter
    private FlightResult flightResult;

    private FlightResultWrapper(FlightResult flightResult) {
        this.flightResult = flightResult;
    }

    public static FlightResultWrapper from(FlightResult flightResult) {
        return new FlightResultWrapper(flightResult);
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FlightResultWrapper that = (FlightResultWrapper) o;

        return Objects.equals(flightResult.getSupplier_IMPORTANT_PRIMITIVE_FIELD(), that.flightResult.getSupplier_IMPORTANT_PRIMITIVE_FIELD())
            && Objects.equals(flightResult.getDiscount_IMPORTANT_FIELD_OBJECT().getPercentage(), that.getFlightResult().getDiscount_IMPORTANT_FIELD_OBJECT().getPercentage());
    }

    @Override
    public int hashCode() {
        return Objects.hash(
                flightResult.getSupplier_IMPORTANT_PRIMITIVE_FIELD(),
                flightResult.getDiscount_IMPORTANT_FIELD_OBJECT().getPercentage());
    }
}
